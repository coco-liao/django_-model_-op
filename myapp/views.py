from django.shortcuts import render
from django.http import HttpResponse
from .models import Users


# Create your views here.
def index(request):
    return HttpResponse('<h1>首页</h1><a href="/users">用户信息管理</a>')


# 浏览用户信息
def index_users(request):
    try:
        users = Users.objects.all()
        context = {'userslist': users}
        return render(request, "myapp/users/index.html", context)
    except:
        return HttpResponse('没有找到用户信息！')


# 添加用户
def add_users(request):
    return render(request, "myapp/users/add.html")


# 执行添加(表单相关)
def insert_users(request):
    try:
        ob = Users()
        ob.name = request.POST['name']
        ob.age = request.POST['age']
        ob.phone = request.POST['phone']
        ob.save()
        context = {'info': '添加成功'}
    except:
        context = {'info': '添加失败'}
    return render(request, "myapp/users/info.html", context)


# 删除用户,为什么要传参数？
def del_users(request, uid=0):
    try:
        ob = Users.objects.get(id=uid)
        ob.delete()
        context = {'info': '删除成功'}
    except:
        context = {'info': '删除失败'}
    return render(request, "myapp/users/info.html", context)


# 编辑用户信息
def edit_users(request, uid=0):
    try:
        ob = Users.objects.get(id=uid)
        context = {'user': ob}
        return render(request, "myapp/users/edit.html", context)
    except:
        context = {'info': '加载修改数据异常！'}
        return render(request, "myapp/users/info.html", context)


# 执行编辑(表单相关)
def update_users(request):
    try:
        ob = Users.objects.get(id=request.POST['id'])
        ob.name = request.POST['name']
        ob.age = request.POST['age']
        ob.phone = request.POST['phone']
        ob.save()
        context = {'info': '修改成功'}
    except:
        context = {'info': '修改失败'}
    return render(request, "myapp/users/info.html", context)
